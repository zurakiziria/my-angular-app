angular.module('employee', ['mFinder']);
angular.module('employee')
        .component('employeeComponent', {
            templateUrl: 'app/html/employee/default.html',

            controllerAs: 'vv',
            bindings: {$router: '<'},
            controller: ['itemsService', '$uibModal', 'companies', function (itemsService, $uibModal, companies) {

                    var self = this;


                    self.elisttype = {};
                    self.elist = {};
                    self.finedDrugTransfers = new Array();
                    self.group = true;
                    self.tid = 0;
                    self.relatedItems = [];
                    self.comp_id = 0;
                    this.$routerOnActivate = function (next) {
                        self.loadTypes();
                        self.tid = next.params.id;

                        self.loadItem(next.params.id);


                        companies.getCompanySess().then(function (data) {


                            if (data.data === null) {
                                self.$router.navigate(['Mycompanies']);
                            } else {
                                self.comp_id = data.data;
                            }


                        });


                    }
                    this.$onInit = function () {

                    };




                    self.goToElist = function (id) {
                        self.$router.navigate(['Employee', {id: id}]);
                    };
                    self.goToItem = function (id) {
                        console.log(id)
                        self.$router.navigate(['Item', {id: id}]);
                    };
                    self.loadTypes = function () {
                        itemsService.eListType().then(function (data) {

                            self.elisttype = data.data;


                        });
                    }



                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };

                    self.loadItem = function (id) {
                        itemsService.eList(id).then(function (data) {

                            if (data.data.length > 0) {
                                self.elist = data.data;
                            }

                        });
                    }

                    self.refresh = function () {
                        self.loadTypes();
                        self.loadItem(self.tid);

                    };

                    self.getRelatedItems = function (itemId)
                    {
                        itemsService.getRelatedEmployeeItems(itemId).then(function (data) {
                            if (data.data.status)
                                self.relatedItems = data.data.items;
                        });
                    };

                    self.checknumber = function () {
                        var reg = new RegExp('^[0-9]+$');
                        if (!reg.test(modalm.i_number)) {
                            modalm.message = 'Write employee name';
                        } else {
                            modalm.message = null;
                        }

                    }

                    self.deleteEmployee = function (item) {
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/employee/employeedel.html',
                            controllerAs: 'modalm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.onSubmit = function () {

                                        var toSend = {};
                                        toSend.id = item.id;
                                        itemsService.delEmployee(toSend).then(function (data) {

                                            if (data.data.status)
                                            {
                                                self.message = data.data.message;
                                                self.refresh();
                                                self.closePopup();
                                            } else {
                                                modalm.message = data.data.message;
                                            }
                                        });
                                    };

                                    modalm.cancel = function () {
                                        self.closePopup();
                                    }

                                }]
                        });
                    };

                    self.editEmployee = function (item) {
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/employee/editemployee.html',
                            controllerAs: 'modalm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;


                                    modalm.name = item.title;
                                    modalm.i_number = item.i_number;
                                    modalm.position = item.position;

                                    modalm.checknumber = function () {
                                        self.checknumber();
                                    }

                                    modalm.onSubmit = function () {

                                        if (!modalm.name || modalm.name === '')
                                        {
                                            modalm.message = 'ჩაწერეთ თანამშრომლის სახელი';
                                            return false;
                                        }
                                        if (!modalm.i_number || modalm.i_number === '')
                                        {
                                            modalm.message = 'ჩაწერეთ პირადობის ნომერი';
                                            return false;
                                        }
                                        if (modalm.i_number.toString().length != 11)
                                        {
                                            modalm.message = 'პირადობის ნომერი უნდა იყო 11 ნიშნა რიცხვი';
                                            return false;
                                        }
                                        if (!modalm.position || modalm.position === '')
                                        {
                                            modalm.message = 'ჩაწერეთ თანამშრომლის პოზიცია';
                                            return false;
                                        }
                                        var toSend = {};
                                        toSend.id = item.id;
                                        toSend.title = modalm.name;
                                        toSend.i_number = modalm.i_number;
                                        toSend.position = modalm.position;
                                        toSend.state = 1;
                                        toSend.company_id = self.comp_id;

                                        itemsService.saveEmployee(toSend).then(function (data) {

                                            if (data.data.status)
                                            {
                                                self.message = data.data.message;
                                                self.refresh();
                                                self.closePopup();
                                            } else {
                                                modalm.message = data.data.message;
                                            }
                                        });
                                    };

                                    modalm.cancel = function () {
                                        self.closePopup();
                                    }
                                }]
                        });

                    }

                    self.EmployeeItemRel = function (item) {
                        self.popupOen = $uibModal.open({
                            template: '<employee-modal modal-on-save="modalm.reload()" modal-item="modalm.item"></employee-modal>',
                            controllerAs: 'modalm',

                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.selectedType = null;
                                    modalm.item = item;
                                    modalm.reload = function () {
                                        //self.getRelatedItems(item.id);
                                    };



                                }]
                        });
                    };

                    self.deleteEmployeeRelatedItem = function (item, eid) {
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/item/eventdel.html',
                            controllerAs: 'modalm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;

                                    var frel = {};
                                    frel.id = eid;
                                    modalm.onSubmit = function () {

                                        var toSend = {};

                                        toSend.dest = item.id;
                                        toSend.source = eid;
                                        itemsService.deleteEmployeeRelatedItem(toSend).then(function (data) {
                                            if (data.data.status)
                                            {
                                                //        self.message = data.data.message;

                                                self.closePopup();
                                                self.RelItems(frel);
                                            } else {
                                                //       modalm.message = data.data.message;
                                            }
                                        });
                                    };

                                    modalm.cancel = function () {

                                        self.closePopup();

                                        self.RelItems(frel);
                                    }
                                }]
                        });
                    };

                    self.RelItems = function (item) {

                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/js/components/employee/modal/list.html',
                            controllerAs: 'modalm',

                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.selectedType = null;
                                    modalm.item = item;

                                    modalm.relatedItems = []
                                    itemsService.getRelatedEmployeeItems(item.id).then(function (data) {

                                        if (data.data.status)
                                            modalm.relatedItems = data.data.items;
                                    });

                                    modalm.cancel = function () {
                                        self.closePopup();
                                    }

                                    modalm.goToItem = function (id) {
                                        self.goToItem(id);
                                    }

                                    modalm.deleteRelatedItem = function (item, eid) {
                                        self.closePopup();
                                        self.deleteEmployeeRelatedItem(item, eid);

                                    }
                                }]
                        });

                    };
                    self.getHistory = function (item) {
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/employee/history.html',
                            controllerAs: 'modalm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;

                                   modalm.item = item;
                                   modalm.employ = [];
                                   
                                   
                                   itemsService.getEmployeeHistory(item).then(function (data) {
                                      modalm.employ = data.data.data; 
                                      
                                   }
                                   )
                                   
                                }]
                        });
                    };
                }]
        })