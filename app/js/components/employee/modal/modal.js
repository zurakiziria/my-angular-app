angular.module('employeeModal', []);
angular.module('employeeModal')
        .component('employeeModal', {
            templateUrl: 'app/js/components/employee/modal/default.html',

            controllerAs: 'vv',
            bindings: {modalItem: '<?',
                modalOnSave: '&'
            },
            controller: ['itemsService', 'categories', function (itemsService, categories) {

                    var self = this;
                    self.userInfoMessage = '';
                    self.userInfoError = '';
                    self.filters = {
                        catid: ''
                        , title: ''
                    };
                    self.pagination = {

                        "pagesTotal": 20,
                        "total": 600,
                        "limit": 2,
                    };
                    self.pagesCurrent = 1;



                    this.$onInit = function () {
                        self.loadCategories();
                        self.loadItems();
                    };

                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };

                    self.loadCategories = function () {

                        categories.getCategories().then(function (data) {

                       
                            self.company = data.data.company;

                            self.company.categories.unshift({id: '', title: 'აირჩიე კატეგორია'});


                        });

                    };

                    self.loadItems = function () {
                        
                        itemsService.getModalData({filters: self.filters, pagination: self.pagination}).then(function (data) {
                           
                            self.items = data.data.items;
                            self.pagination = data.data.pagination;

                        });

                    };

                    self.filter = function () {
                        self.loadItems();
                    };

                    self.pageChanged = function () {
                        // self.$router.navigate(['Categoryp', {id: self.categoryId, page: self.pagesCurrent}]);
                        self.pagination.page = self.pagesCurrent;
                        self.loadItems();
                    };

                    self.linkItem = function (item) {
                        console.log(self.modalItem)
                        var data = {sourceId: self.modalItem.id
                            , destid: item.id
                        };
                        itemsService.linkEmployeeItem(data).then(function (data) {

                            if (data.data.status)
                            {
                                item.successMessage = data.data.message;
                                self.modalOnSave();
                            } else {
                                item.errorMessage = data.data.message;
                            }


                        });

                    };


                }]


        });