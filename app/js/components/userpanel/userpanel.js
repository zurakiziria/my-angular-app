angular.module('userPanel', []);
angular.module('userPanel')
        .component('userPanelComponent', {
            templateUrl: 'app/html/user/panel.html',
            controllerAs: 'vv',

            bindings: {$router: '<'},
            controller: ['itemsService', '$window', function (itemsService, $window) {

                    var self = this;
                    self.user = {};
                    self.state = false;
                    self.$onInit = function () {
                        itemsService.getUser().then(function (data) {
                            self.user = data.data;
                        });
                    };
                    self.logout = function () {
                        var host = $window.location.host;
                        var landingUrl = "http://" + host + "/index.php";
                        itemsService.logout().then(function () {
                            $window.location.href = landingUrl;
                        });


                    };
                }]


        });