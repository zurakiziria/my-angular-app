angular.module('categoryMap', ['aurbano.multiselect']);
angular.module('categoryMap')
        .component('categoryMap', {
            templateUrl: 'app/js/components/map/default.html',
            controllerAs: 'vv',
            bindings: {$router: '<'},
            controller: ['itemsService', 'categories', '$scope', '$sce', function (itemsService, categories, $scope, $sce) {
                    var self = this;


                    $scope.multiselect = {
                        selected: [],
                        options: [],
                        config: {
                            hideOnBlur: false,
                            showSelected: false,
                            itemTemplate: function (item) {
                                return $sce.trustAsHtml(item.name + ' ' + item.email + '');
                            },
                            labelTemplate: function (item) {
                                var add = '';
                                if (item.email) {
                                    add = ' ' + item.email + ' ';
                                }
                                return $sce.trustAsHtml(add + item.name);
                            }
                        }
                    };
                    $scope.displayUsers = function () {
                        return $scope.multiselect.selected.map(function (each) {
                            return each.name;
                        }).join(', ');
                    };
                    categories.getCategories().then(function (data) {


                        $scope.multiselect.options = data.data.company.categories.map(function (item) {
                            var paramsc = self.getParams(item.params);
                            if (self.hasImage(paramsc)) {
                                var iconc = "<img src='" + paramsc.m_image + "'/>";
                            } else {
                                var iconc = "";
                            }
                            return {

                                name: item.title,
                                email: iconc,
                                id: item.id
                            };
                        });

                    });
                    categories.getCategories().then(function (data) {

                        self.user = data.data.user;
                        self.company = data.data.company;
                    });
                    var mapOptions = {
                        zoom: 7,
                        center: new google.maps.LatLng(41.69411, 44.83368),
                        mapTypeId: 'roadmap'

                    }

                    $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
                    $scope.markers = [];
                    var infoWindow = new google.maps.InfoWindow();
                    var createMarker = function (info) {

                        var params = self.getParams(info.catparams);
                        if (self.hasImage(params)) {
                            var icon = "" + params.m_image + "";
                        } else {
                            var icon = "";
                        }
                      //  if (info.coords.latitude && info.coords.longitude){
                      console.log(info.coords)
                        var marker = new google.maps.Marker({
                            map: $scope.map,
                            position: new google.maps.LatLng(info.coords.latitude, info.coords.longitude),
                            title: info.title,
                            icon: icon
                        });
                  //  }else{
                       // var marker = 0;
                   // }
                        google.maps.event.addListener(marker, 'click', function () {
                            marker.content = '<div class="infoWindowContent">';
                            setTimeout(function () {

                                itemsService.getImages(info.id).then(function (data) {

                                    // var content = '';
                                    if (data.data.allimages && data.data.allimages.length > 0) {
                                        marker.content += '<div class="imageforgooglemap"><img id="img_c' + info.id + '" src="' + data.data.allimages[0] + '" width="500" height="310"/></div>';
                                        for (m = 0; m < data.data.allimages.length; m++) {
                                            marker.content += '<div class="imageforgooglemaps"><img src="' + data.data.allimages[m] + '" width="60" height="37" onClick="getSrc(\'' + data.data.allimages[m] + '\',' + info.id + ')"/></div>';
                                        }
                                    }


                                });
                                categories.getItemParamses(info.id, info.catid).then(function (data) {
                                    marker.content += '<div>';
                                    marker.content += '<span>იდენტიფიკატორი</span>: ';
                                    marker.content += '<span>' + info.id + '</span>';
                                    marker.content += '</div>';
                                    if (data.data) {
                                        for (tt = 0; tt < data.data.length; tt++) {
                                            marker.content += '<div>';
                                            marker.content += '<span>' + data.data[tt].vl_name + '</span>: ';
                                            if (data.data[tt].vl_opname || data.data[tt].vl_text_value) {
                                                if (data.data[tt].vl_opname) {
                                                    marker.content += '<span>' + data.data[tt].vl_opname + '</span>';
                                                }
                                                if (data.data[tt].vl_text_value) {
                                                    marker.content += '<span>' + data.data[tt].vl_text_value + '</span>';
                                                }
                                            }
                                            marker.content += '</div>';
                                        }
                                    }

                                });
                            }, 300);
//                                if (info.images) {
//                                    marker.content += '<div class="imageforgooglemap"><img id="img_c'+info.id+'" src="'+info.images[0]+'" width="500" height="310"/></div>';
//                                    for (ii = 0; ii < info.images.length; ii++) {
//                                        marker.content += '<div class="imageforgooglemaps"><img src="'+info.images[ii]+'" width="60" height="37" onClick="getSrc(\''+info.images[ii]+'\','+info.id+')"/></div>';
//                                    }
//                                }


                            setTimeout(function () {
                                marker.content += '<div>';
                                marker.content += '<span>საწყისი ღირებულება</span>: ';
                                if (info.start_price) {
                                    marker.content += '<span>' + info.start_price + '</span>';
                                }
                                marker.content += '</div>';
                                marker.content += '<div>';
                                marker.content += '<span>საბოლოო ღირებულება</span>: ';
                                marker.content += '<span>' + info.end_price + '</span>';
                                marker.content += '</div>';
                                marker.content += '<div>';
                                marker.content += '<span>თარიღი</span>: ';
                                marker.content += '<span>' + info.created + '</span>';
                                marker.content += '</div>';
                                marker.content += '</div>';
                                infoWindow.setContent('<a href="app.html#!/item/' + info.id + '" class="myclass_' + info.id + '" target="blank">' + marker.title + '</a> ' + marker.content);
                                infoWindow.open($scope.map, marker);
                            }, 1000);
                        });
                        $scope.markers.push(marker);
                    }
                    self.getcat = function (cid) {

                        if (cid) {
                            categories.getMapItems(cid).then(function (data) {

                                self.clearOverlays = function () {
                                    for (var i = 0; i < $scope.markers.length; i++) {
                                        $scope.markers[i].setMap(null);
                                    }
                                    $scope.markers.length = 0;
                                }
                                self.clearOverlays();
                                if (data.data.items.length)
                                {
                                    for (i = 0; i < data.data.items.length; i++) {

                                        createMarker(data.data.items[i]);
                                    }
                                }

                            });
                        }
                    }

                    this.$onInit = function () {
                        categories.getMapItems().then(function (data) {

                            self.clearOverlays = function () {
                                for (var i = 0; i < $scope.markers.length; i++) {
                                    $scope.markers[i].setMap(null);
                                }
                                $scope.markers.length = 0;
                            }
                            self.clearOverlays();
                            if (data.data.items.length)
                            {
                                for (i = 0; i < data.data.items.length; i++) {

                                    createMarker(data.data.items[i]);
                                }
                            }

                        });
                    }
                    $scope.openInfoWindow = function (e, selectedMarker) {
                        e.preventDefault();
                        google.maps.event.trigger(selectedMarker, 'click');
                    }


                    self.getParams = function (item) {

                        item = angular.fromJson(item);

                        return item;
                    };
                    self.hasImage = function (category)
                    {
                        return category.m_image ? true : false;
                    };


//Angular App Module and Controller




                    /*
                     self.user = {};
                     self.windowOptions = {
                     visible: false,
                     pixelOffset: {
                     height: -25,
                     width: 0
                     }
                     };
                     
                     self.map = {
                     center: {
                     latitude: 41.69411,
                     longitude: 44.83368
                     },
                     zoom: 7,
                     bounds: {
                     northeast: {
                     latitude: 41,
                     longitude: 45
                     },
                     southwest: {
                     latitude: 40,
                     longitude: 44
                     }
                     }
                     };
                     
                     self.marker = {
                     id: 0,
                     coords: {
                     latitude: 40.1451,
                     longitude: -99.6680
                     },
                     options: {"draggable": true, labelContent: "lat"}
                     };
                     
                     self.options = {
                     scrollwheel: true
                     };
                     var createRandomMarker = function (i, bounds, idKey) {
                     var lat_min = bounds.southwest.latitude,
                     lat_range = bounds.northeast.latitude - lat_min,
                     lng_min = bounds.southwest.longitude,
                     lng_range = bounds.northeast.longitude - lng_min;
                     
                     if (idKey == null) {
                     idKey = "id";
                     }
                     
                     var latitude = lat_min + (Math.random() * lat_range);
                     var longitude = lng_min + (Math.random() * lng_range);
                     var ret = {
                     
                     coords: {
                     latitude: latitude,
                     longitude: longitude,
                     },
                     title: 'm' + i,
                     labelContent: 'sdsdf',
                     events: '',
                     options: ''
                     
                     
                     };
                     console.log(ret);
                     ret[idKey] = i;
                     return ret;
                     };
                     
                     self.places = [
                     {
                     idKey: 583187,
                     latitude: 46.7682,
                     longitude: -71.3234,
                     title: "title"
                     }
                     ];
                     //                    var markers = [];
                     //                    for (var i = 0; i < 50; i++) {
                     //                        markers.push(createRandomMarker(i, self.map.bounds))
                     //                    }
                     
                     
                     
                     this.$onInit = function () {
                     
                     categories.getCategories().then(function (data) {
                     
                     self.user = data.data.user;
                     self.company = data.data.company;
                     });
                     
                     
                     
                     
                     categories.getMapItems().then(function (data) {
                     if (data.data.items.length)
                     {
                     self.randomMarkers = data.data.items.map(function (a) {
                     a.allimages = [];
                     //      if (typeof a.images !== 'undefined') {
                     console.log(a);
                     //                                        itemsService.getImages(a.id).then(function (data) {
                     //
                     //
                     //
                     //                                            console.log('work');
                     //                                            if (data.data.allimages.length > 0 && data.data.status)
                     //                                            {
                     //                                                for (m = 0; m < data.data.allimages.length; m++) {
                     //
                     //
                     //                                                    var obj = {
                     //                                                        src: '' + data.data.allimages[m] + ''
                     //                                                    };
                     //
                     //                                                    a.allimages.push(obj);
                     //
                     //
                     //                                                }
                     //
                     //                                            }
                     //
                     //                                        });
                     //     }
                     //                                    if (a.paramses.length > 0)
                     //                                    {
                     //                                        a.string_paramses = '';
                     //
                     //                                        for (i = 0; i < a.paramses.length; i++)
                     //                                        {
                     //                                            if (a.paramses[i].vl_opname != null || a.paramses[i].vl_text_value != null) {
                     //                                                if (a.paramses[i].vl_opname == null) {
                     //                                                    a.string_paramses += "<div>" + a.paramses[i].vl_name + ": " + a.paramses[i].vl_text_value + ". </div>";
                     //                                                } else {
                     //                                                    a.string_paramses += "<div>" + a.paramses[i].vl_name + ": " + a.paramses[i].vl_opname + ". </div>";
                     //                                                }
                     //                                            } else {
                     //                                                a.string_paramses += "<div>" + a.paramses[i].vl_name + ":  </div>";
                     //                                            }
                     //
                     //                                        }
                     //                                    }
                     
                     return a;
                     });
                     
                     
                     }
                     });
                     
                     };
                     
                     self.takeid = 0;
                     self.takecatid = 0;
                     self.taketitle = 0;
                     self.takecoords = 0;
                     self.hideicon = true;
                     self.markeditem = function (par, id) {
                     console.log(par + ' ' + id);
                     }
                     
                     self.goToCategory = function (id) {
                     self.$router.navigate(['Category', {id: id}]);
                     };
                     self.clickOnMarker = function (a) {
                     console.log(this)
                     
                     };
                     self.closeClick = function () {
                     self.takeid = 0;
                     self.takecatid = 0;
                     self.taketitle = 0;
                     self.takecoords = 0;
                     self.hideicon = true;
                     self.show = false;
                     };
                     
                     self.goToItem = function (id) {
                     
                     self.$router.navigate(['Item', {id: id}]);
                     };
                     
                     self.openClick = function (id, catid, title, coords) {
                     self.takeid = id;
                     self.takecatid = catid;
                     self.taketitle = title;
                     self.takecoords = coords;
                     self.hideicon = true;
                     self.show = true;
                     
                     //                        console.log(params);
                     //                        return 'sssdddd';
                     };
                     
                     */
                }]



        });


var getSrc = function (src, id) {
    document.getElementById('img_c' + id + '').setAttribute("src", "" + src + "");

}



