(function (angular) {
    angular.module('mFinder', []);
    angular.module('mFinder')
            .component('mFinderComponent', {
                templateUrl: 'app/html/ngfinder/default.html',
                controllerAs: 'vv',
                bindings: {
                    prim: '=',
                    startPath:'=?'
                },
                controller: ['$sce', function ($sce) {
                        var self = this;                                        
                        self.$onInit = function () {
                            
                            self.generated = $sce.trustAsResourceUrl('/elFinder/elfinder.php?mdir=' + self.prim+'&startPath='+(self.startPath?self.startPath:''));
                        };
                    }]
            });
})(angular);