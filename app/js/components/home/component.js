angular.module('home', []);
angular.module('home')
        .component('homeComponent', {
            templateUrl: 'app/html/home/default.html',
            controllerAs: 'vv',
            bindings: {"$router": '<'},
            controller: ['categories', function (categories) {
                    var self = this;
                    self.user = {};
                    self.company = {};
                    self.finedDrugTransfers = new Array();
                    self.btn = 0;
                    this.$onInit = function () {
                        categories.getCategories().then(function (data) {

                            self.user = data.data.user;
                            self.company = data.data.company;
                            
                          //  if (data.data.company.id == 2 || data.data.company.id == 30){
                                self.btn = 1;
                          //  }
                        });
                    };
                    self.goToCategory = function (id) {
                        self.$router.navigate(['Category', {id: id}]);
                    };
                    self.goToEmployee = function (id) {
                        self.$router.navigate(['Employee']);
                    };
                    self.getParams = function (item) {
                        if (!item.options)
                        {
                            item.options = angular.fromJson(item.params);
                        }
                        return item.options;
                    };
                    self.hasImage = function (category)
                    {
                        self.getParams(category);
                        return category.options.image ? true : false;
                    };
                }]


        });