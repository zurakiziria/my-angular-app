'use strict';
angular.module('mycompanies', []);
// Register `mycompanies` component, along with its associated controller and template
angular.module('mycompanies')
        .component('myCompanies', {
            templateUrl: 'app/html/mycompanies/default.html',
            controllerAs: 'mm',
            bindings: {"$router": '<'},
            controller: ['companies', function (companies) {
                    var self = this;
                    var cdata = {};
                    var status = true;
                    var activeId = 0;
                    self.$onInit = function () {
                        companies.getCompanySess().then(function (data) {
                            self.activeId = data.data;
                            
                        }
                        );
                        companies.getUserCompanies().then(function (data) {
                            self.cdata = data.data;

                        }
                        );
                    }
                    self.putin = function (id) {
                      
                        if (id) {
                            companies.setCompanySess(id).then(function (data) {

                                if (data.data.id) {
                                    self.$router.navigate(['Home']);
                                }

                            })
                        }
                    }

                }]
        });

