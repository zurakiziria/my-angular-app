angular.module('category', ['ui.bootstrap']);
angular.module('category')
        .component('categoryComponent', {
            templateUrl: 'app/html/category/default.html',
            controllerAs: 'vv',

            bindings: {$router: '<'},
            controller: ['itemsService', '$uibModal', '$translate', function (itemsService, $uibModal, $translate) {


                    var self = this;
                    self.total = 50;

                    self.pagination = {

                        "pagesTotal": 20,
                        "total": 600,
                        "limit": 2,
                    };
                    self.pagesCurrent = 2;
                    self.categoryId = 0;

                    self.items = [];
                    self.group = 0;
                    self.category = {};
                    self.balancesum = {};
                    self.search = {};
                    self.message = null;
                    self.$routerOnActivate = function (next, previos) {
                        //activate is first
                        self.categoryId = next.params.id;
                        if (next.params.page)
                        {
                            self.pagesCurrent = next.params.page;
                        } else {
                            self.pagesCurrent = 1;
                        }

                        itemsService.getUser().then(function (data) {
                            var arraygr = data.data.groups;

                            //  if ('8' in arraygr)
                            // {
                            // element found

                            self.group = 1;//1;

                            // }

                        })
                        var optype = 0;
                        self.searchWord = function () {
                            console.log(self.search.key)
                            setTimeout(function () {
                                if (self.search.oprt) {
                                    optype = 13;
                                } else {
                                    optype = 0;
                                }

                                itemsService.getData({id: self.categoryId, page: self.pagesCurrent, searcht: self.search.word, searchop: optype, searchkey: self.search.key}).then(function (data) {
                                    console.log(data.data)
                                    self.items = data.data.items;
                                    self.category = data.data.category;
                                    self.pagination = data.data.pagination;
                                    self.balancesum = data.data.balancesum;

//                                        self.pageChanged = function () {
//                                            self.$router.navigate(['Categorys', {id: self.categoryId, page: self.pagesCurrent, searcht: self.search.word, searchop: optype, searchkey: self.search.key}]);
//                                        };
                                    self.pageChanged = function () {
                                        self.$router.navigate(['Categoryp', {id: self.categoryId, page: self.pagesCurrent}]);
                                    };

                                });

                            }, 2000);


                        }


                        self.clearSearchWord = function () {
                            itemsService.clearSession({id: self.categoryId}).then(function () {
                                console.log('clare' + self.categoryId);
                                self.search.word = '';
                                self.search.key = '';
                                location.reload();
//                            self.$router.navigate(['Categoryp', {id: self.categoryId, page: self.pagesCurrent}]);
                            });
                        };
                        self.newsearch = {};
                        self.newsearch.key = '';
                        self.newsearch.word = '';

                        if (next.params.searcht) {
                            self.newsearch.word = decodeURIComponent(next.params.searcht);
                            self.newsearch.key = decodeURIComponent(next.params.searchkey);
                        }

                        self.Items();

                    };
                    this.$onInit = function () {

                    };
                    self.goToItem = function (id) {
                        self.$router.navigate(['Item', {id: id}]);
                    };
                    self.Items = function () {
                                                itemsService.getData({id: self.categoryId, page: self.pagesCurrent, searcht: self.newsearch.word, searchkey: self.newsearch.key}).then(function (data) {
                            if (data.data.session.filterTitle && data.data.session.filterkey) {
                                self.search.word = data.data.session.filterTitle;
                                self.search.key = data.data.session.filterkey;
                            }
                            self.additionals = data.data.category.itemAddMapSchema.properties;
                            //delete self.additionals.name;
                            //delete self.additionals.price;
                            delete self.additionals.verbal_address;
                            delete self.additionals.depreciation;
                            delete self.additionals.address;
                            //delete self.additionals.bcode;
                            delete self.additionals.coords;
                            console.log(data.data);
                            self.items = data.data.items;
                            self.category = data.data.category;
                            self.pagination = data.data.pagination;
                            self.balancesum = data.data.balancesum;
//                            if (next.params.searcht) {
//                                self.pageChanged = function () {
//                                    self.$router.navigate(['Categorys', {id: self.categoryId, page: self.pagesCurrent, searcht: self.newsearch.word, searchkey: self.newsearch.key}]);
//                                };
//                            } else {
                            self.pageChanged = function () {
                                self.$router.navigate(['Categoryp', {id: self.categoryId, page: self.pagesCurrent}]);
//                            }
                            }
                        });
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.addItem = function (item) {




                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/category/popupadd.html',
                            controllerAs: 'modalm',

                            controller: ['$scope', '$http', '$uibModal', 'schemaForm', function ($scope, $http, $uibModal, schemaForm) {
                                    var modalm = this;
                                    modalm.transfers = [];
                                    modalm.close = function () {
                                        self.closePopup();
                                    };
                                    $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
                                    modalm.schema = self.category.itemAddMapSchema;
                                    modalm.form = self.category.itemAddMap;
                                    console.log(self.category);
                                    modalm.model = {};
                                    if (typeof (item) !== 'undefined')
                                    {
                                        itemsService.getFormData(item.id).then(function (data) {
                                            if (data.data.status)
                                            {
                                                modalm.model = data.data.item;
                                            }
                                        });
                                    }
                                    modalm.onSubmit = function (form) {
                                        // First we broadcast an event so all fields validate themselves
                                        $scope.$broadcast('schemaFormValidate');
                                        //console.log(modalm.model);return;
                                        // Then we check if the form is valid
                                        if (form.$valid) {
                                            // ... do whatever you need to do with your data.
                                            modalm.model.category_rel_id = self.category.id;
                                            modalm.model.rel_catid = self.category.relation_id;

                                            itemsService.saveItem(modalm.model).then(function (data) {
                                                if (data.data.status)
                                                {
                                                    self.message = data.data.message;
                                                    self.closePopup();
                                                } else {
                                                    self.message = data.data.message;
                                                    self.closePopup();
                                                }
                                            });
                                        }
                                    };
                                }]
                        });
                    };


                    self.deleteItem = function (id) {

                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/employee/employeedel.html',
                            controllerAs: 'modalm',

                            controller: ['$scope', '$http', '$uibModal', 'schemaForm', function ($scope, $http, $uibModal, schemaForm) {
                                    var modalm = this;
                                    modalm.onSubmit = function () {
                                        console.log('ondelete')
                                        var toSend = {};
                                        toSend.id = id;
                                        itemsService.deleteItem(toSend).then(function (data) {
                                            console.log(data)
                                            if (data.data.status)
                                            {
                                                self.message = data.data.message;
                                                self.Items();
                                                self.closePopup();
                                            } else {
                                                modalm.message = data.data.message;
                                                self.closePopup();
                                            }
                                        });
                                    };
                                    modalm.cancel = function () {
                                        self.closePopup();
                                    };
                                }]
                        })
                    }
                }]
        });




      