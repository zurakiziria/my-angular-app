angular.module('zlist', []);
angular.module('zlist')
        .component('zuraTestList', {
            templateUrl: 'app/html/zura/list/default.html',
            controllerAs: 'zl',
            controller: ('zuralistcontroller', ['$scope', '$http', 'itemsService', function ($scope, $http, itemsService) {
                    var self = this;
                    self.items = '';
                    this.$onInit = function () {
                        itemsService.getZura().then(function (data) {
                            self.items = data.data.items;
                        });
                    }


                }])
        });


