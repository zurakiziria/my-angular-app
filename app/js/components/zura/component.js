angular.module('zura', []);
angular.module('zura')
        .component('zuraTest', {
            templateUrl: 'app/html/zura/default.html',
            controllerAs: 'zl',
            controller: ['$scope', '$http', 'itemsService', function ($scope, $http, itemsService) {
                    var self = this;
                    self.canclick = true;
                    var id = 0;

                    var forSave = {};

                    var date = new Date();
                    var day = date.getDate();
                    var monthIndex = date.getMonth();
                    var year = date.getFullYear();

                    $scope.ztext = null;
                    $scope.zdescription = null;

                    
                    itemsService.getUser().then(function (data) {
                        self.userid = data.data.id;
                    }
                    );
//get items array();


                    self.getItems = function (id) {
                        itemsService.getZura(id).then(function (data) {
                            if (id) {
                                $scope.ztext = data.data.items['0'].title;
                                $scope.zdescription = data.data.items['0'].description;

                            }
                            self.items = data.data.items;
                        });
                    };

                    self.$routerOnActivate = function (next, previos) {
                        self.itemId = next.params.id;

                        self.getItems(self.itemId);

                    };

                    $scope.puton = function () {

                        if ($scope.ztext) {

                            forSave.title = $scope.ztext;
                            forSave.description = $scope.zdescription;
                            forSave.userid = self.userid;
                            forSave.itemid = 0;
                            if (self.itemId) {
                                var id = self.itemId;
                                forSave.itemid = self.itemId;
                            }

                            itemsService.saveZura(forSave).then(function (data) {

                                itemsService.getZura(id).then(function (data) {
                                    if (!id) {
                                        $scope.ztext = null;
                                        $scope.zdescription = null;
                                    }
                                    self.items = data.data.items

                                });

                            }
                            );

                        } else {
                            $scope.name = self.nothing();
                        }
                    }
                    self.nothing = function () {
                        alert('empty');
                    }
                }]

        }
        );

