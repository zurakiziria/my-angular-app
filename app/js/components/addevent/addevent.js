angular.module('addEvent', []);
angular.module('addEvent')
        .component('addEventComponent', {
            templateUrl: 'app/html/addevent/addevent.html',
            controllerAs: 'vv',

            bindings: {
                itemid: '=',
                onAdd: '&'
            },
            controller: ['itemsService', function (itemsService) {
                    var modalm = this;
                    modalm.selectedType = null;
                    modalm.types = [];
                    modalm.message = null;
                    modalm.value = null;
                    modalm.description = null;
                    itemsService.getEventsType().then(function (data) {
                        modalm.types = data.data.items;
                    });
                    if (typeof (item) !== 'undefined')
                    {

                    }
                    modalm.resetForm = function () {
                        modalm.selectedType = null;
                        modalm.value = null;
                        modalm.description = null;
                        modalm.date = null;
                    };
                    modalm.onSubmit = function () {

                        if (!modalm.selectedType || modalm.selectedType === '')
                        {
                            modalm.message = 'Select event type';
                            return false;
                        }
                        if (!modalm.value || modalm.value === '')
                        {
                            modalm.message = 'Select value';
                            return false;
                        }
                        if (!modalm.date || modalm.date === '')
                        {
                            modalm.message = 'Select date';
                            return false; 
                        }

                        var toSend = {};
                        toSend.event_id = modalm.selectedType;
                        toSend.item_id = modalm.itemid;
                        toSend.value = modalm.value;
                        toSend.description = modalm.description;
                        toSend.date = modalm.date;
                        itemsService.saveEvent(toSend).then(function (data) {
                            if (data.data.status)
                            {
                                modalm.message = data.data.message;
                                modalm.onAdd();
                                modalm.resetForm();

                            } else {
                                modalm.message = data.data.message;
                            }

                        });

                    };




                }]


        });