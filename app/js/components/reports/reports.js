angular.module('reportsPage', []);
angular.module('reportsPage')
        .component('reportsPage', {
            templateUrl: 'app/js/components/reports/default.html',
            controllerAs: 'vv',

            bindings: {$router: '<'},
            controller: ['itemsService','$scope', 'categories', function (itemsService, $scope, categories) {
                    var self = this;
                   
                    this.$onInit = function () {
                        categories.getCategories().then(function (data) {

                            self.user = data.data.user;
                            self.company = data.data.company;
                            
                            self.company.categories[self.company.categories.length] = {'id':44444,'title':'ყველა'};
                          
                        });


                    };
                    
                    $scope.Change = function(){
                        self.number = [{}];
                        self.selectednumber = '';
                        if (self.selectedCategory){
                            categories.getCategory(self.selectedCategory).then(function (data) {
                               
                                if (data.data.company.category.item_number > 100){
                                  l = 0;
                                    for (i = 1; i <= Math.ceil(data.data.company.category.item_number / 100); i++){
                                        self.number[l] = {'id':i,'title':'გვერდი '+i};
                                        console.log(i);
                                        l++;
                                    }
                                    console.log(self.number);
                                }
                            });
                        }
                    }
                    self.goToCategory = function (id) {
                        self.$router.navigate(['Category', {id: id}]);
                    };

                    self.goToItem = function (id) {
                        conssole.log(id)
                        self.$router.navigate(['Item', {id: id}]);
                    };

                }]

        });