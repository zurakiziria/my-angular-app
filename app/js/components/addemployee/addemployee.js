angular.module('addEmployee', []);
angular.module('addEmployee')
        .component('addEmployeeComponent', {
            templateUrl: 'app/html/addemployee/addemployee.html',
            controllerAs: 'vv',

            bindings: {
                itemid: '=',
                onAdd: '&'
            },
            controller: ['itemsService','companies', function (itemsService,companies) {
                    var modalm = this;

                    modalm.maxlength = 11;

                    modalm.name = null;
                    modalm.i_number = null;
                    modalm.position = null;
                    modalm.comp_id = 0;
                    modalm.resetForm = function () {
                        modalm.name = null;
                        modalm.i_number = null;
                        modalm.position = null;

                    };
                    
                                            companies.getCompanySess().then(function (data) {


                            if (data.data === null) {
//                                modalm.$router.navigate(['Mycompanies']);
                            }else{
                                modalm.comp_id = data.data;
                            }


                        });

                    modalm.checknumber = function () {
                        var reg = new RegExp('^[0-9]+$');
                        if (!reg.test(modalm.i_number)) {
                            modalm.message = 'პირადი ნომერი უნდა იყოს მხოლოდ ციფრები';
                        } else {
                            modalm.message = null;
                        }

                    }

                    modalm.onSubmit = function () {
                        if (!modalm.name || modalm.name === '')
                        {
                            modalm.message = 'ჩაწერეთ თანამშრომლის სახელი';
                            return false;
                        }
                        if (!modalm.i_number || modalm.i_number === '')
                        {
                            modalm.message = 'ჩაწერეთ თანამშრომლის პირადი ნომერი';
                            return false;
                        }

                        if (modalm.i_number.toString().length != 11)
                        {
                            modalm.message = 'პირადობის ნომერი უნდა იყო 11 ნიშნა რიცხვი';
                            return false;
                        }
                        if (!modalm.position || modalm.position === '')
                        {
                            modalm.message = 'ჩაწერეთ თანამშრომლის პოზიცია';
                            return false;
                        }

                        var toSend = {};
                        toSend.id = 0;
                        toSend.title = modalm.name;
                        toSend.i_number = modalm.i_number;
                        toSend.position = modalm.position;
                        toSend.state = 1;
                        toSend.type_id = modalm.itemid;
                        toSend.company_id = modalm.comp_id;

                        itemsService.saveEmployee(toSend).then(function (data) {
                            console.log(data.data.message);
                            if (data.data.status)
                            {
                                modalm.message = data.data.message;
                                modalm.onAdd();
                                modalm.resetForm();

                            } else {
                                modalm.message = data.data.message;
                            }

                        });
                    }
                }]


        });