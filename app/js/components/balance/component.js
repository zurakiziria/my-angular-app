angular.module('balancePage', []);
angular.module('balancePage')
        .component('balancePage', {
            templateUrl: 'app/html/balance/default.html',
            controllerAs: 'vv',
            bindings: {$router: '<'},
            controller: ['itemsService', '$scope', 'categories', function (itemsService, $scope, categories) {
                    var self = this;
                    var send = {}

                    this.$onInit = function () {
                        categories.getCategories().then(function (data) {

                            self.user = data.data.user;
                            self.company = data.data.company;

                            

                        });


                    };
                    send.from = self.date_from;
                    send.to = self.date_to;
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }

                    if (mm < 10) {
                        mm = '0' + mm
                    }

                    today = yyyy + '-' + mm + '-' + dd;
                    $scope.maxDate = today;
                    $scope.lastday = 30;
                    $scope.Change = function () {
                        console.log('to: ' + self.data_to + ' from: ' + self.data_from + ' today: ' + today + 'ff');
                        /*                   if (self.data_to == null) {
                         self.data_to = today;
                         }
                         if (self.data_from > self.data_to && self.data_from < today){
                         self.data_to = today;
                         } */

                        if (self.selectQuarter == 1 || self.selectQuarter == 10) {
                            $scope.lastday = 31;
                        } else {
                            $scope.lastday = 30;
                        }
                        $scope.setdate = '';
                        if (self.selectQuarter) {
                           // $scope.setdate = '' + self.selectYear + '-' + self.selectQuarter + '-01';
                            $scope.setdate = '' + self.selectYear + '-01-01';
                            $scope.setdateto = '' + self.selectYear + '-' + (self.selectQuarter + 2) + '-' + $scope.lastday;
                        }
                    }
                    $scope.Changeyear = function () {
                        if (self.selectYear) {
                            $scope.setdate = '' + self.selectYear + '-01-01';
                            if (yyyy == self.selectYear) {
                                $scope.setdateto = today;
                            } else {
                                $scope.setdateto = '' + self.selectYear + '-12-31';
                            }
                        }
                    }
                    itemsService.getYearsByEvent().then(function (data) {
                        console.log(data.data.min);
                        first = data.data.min.substring(0, 4);
                        second = data.data.max.substring(0, 4);
                        arr = Array();
                        for (i = first; i <= second; i++)
                            arr.push(i);
                        $scope.yearlist = arr;
                    });
                    var mon = new Array();
                    mon[0] = {'id': '1', 'name': 'I - quarter'};
                    mon[1] = {'id': '4', 'name': 'II - quarter'};
                    mon[2] = {'id': '7', 'name': 'III - quarter'};
                    mon[3] = {'id': '10', 'name': 'IV - quarter'};
                    console.log(mon.name);
                    $scope.quarter = mon;

                    self.goToCategory = function (id) {
                        self.$router.navigate(['Category', {id: id}]);
                    };
                }]


        });

