'use strict';
angular.module('mainComponent', ['ngComponentRouter', 'home', 'category', 'item', 'mycompanies', 'employee']);
// Register `phoneList` component, along with its associated controller and template
angular.module('mainComponent')
        .config(function ($locationProvider) {
            $locationProvider.html5Mode(false);
        })

        .value('$routerRootComponent', 'mainComponent')
        .component('mainComponent', {
            templateUrl: 'app/html/main_component/default.html',
            $routeConfig: [
                {path: '/mycompanies', name: 'Mycompanies', component: 'myCompanies', useAsDefault: true},
                {path: '/home', name: 'Home', component: 'homeComponent'},
                {path: '/reports', name: 'Reports', component: 'reportsPage'},
                {path: '/balance', name: 'Balance', component: 'balancePage'},
                {path: '/map', name: 'Map', component: 'categoryMap'},
                {path: '/category/:id', name: 'Category', component: 'categoryComponent'},
                {path: '/category/:id/:page', name: 'Categoryp', component: 'categoryComponent'},
                {path: '/category/:id/:page/:searcht', name: 'Categorys', component: 'categoryComponent'},

                {path: '/item/:id', name: 'Item', component: 'itemComponent'},
                
                {path: '/employee', name: 'Employee', component: 'employeeComponent'},
                {path: '/employee/:id', name: 'Employeec', component: 'employeeComponent'},
            ]



        });

