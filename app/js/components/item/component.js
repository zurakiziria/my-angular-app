angular.module('item', ['mFinder']);
angular.module('item')
        .component('itemComponent', {
            templateUrl: function ($element, $attrs, $rootRouter, itemsService) {
                return itemsService.getTemplate();
            },
            $canActivate: function ($nextInstruction, $q, $rootRouter, itemsService) {
                var tt = this;
                tt.itemId = $nextInstruction.params.id;
                return itemsService.getItem(tt.itemId).then(function (data) {
                    tt.item = data.data.item;
                    return true;
                });
            },

            controllerAs: 'vv',
            bindings: {$router: '<'},
            controller: ['itemsService', '$uibModal', function (itemsService, $uibModal) {

                    var self = this;
                    self.itemId = 0;
                    self.item = [];
                    self.item = {};
                    self.message = null;
                    self.events = [];
                    self.userInfoMessage = '';
                    self.userInfoError = '';
                    self.relatedItems = [];
                    self.group = 0;

                    self.getTemplate = function () {
                        //itemsService.getItem(self.itemId);
                        return  'app/html/item/default.html';
                    };
                    self.$routerOnActivate = function (next, previos) {
                        self.itemId = next.params.id;
                        self.loadItem();
                        self.getItemEvents();
                        self.getRelatedItems();
                        self.getEmployeeByItem();
                    };
                    itemsService.getUser().then(function (data) {
                        var arraygr = data.data.groups;

                     //   if ('8' in arraygr)
                      //  {
                            // element found

                            self.group = 1;

                     //   }

                    })
                    self.loadItem = function (reload) {

                        itemsService.getItem(self.itemId, reload).then(function (data) {
                            self.item = data.data.item;
                            self.category = data.data.category;
                            console.log('-- compjs --');
                            console.log(data.data.category);
                            console.log('-- compjs --');
                            if (self.item.coords)
                            {

                                var splitcoord = self.item.coords.split("|");
                                var lat = splitcoord[0];
                                var lng = splitcoord[1];
//                                /map code here
                                self.marker = {
                                    id: 0,
                                    coords: {
                                        latitude: lat,
                                        longitude: lng
                                    },
                                    options: {"draggable": true, labelContent: "lat"}
                                };

                                self.map = {
                                    center: {
                                        latitude: lat,
                                        longitude: lng
                                    },
                                    zoom: 7,
                                    bounds: {
                                        northeast: {
                                            latitude: 41,
                                            longitude: 45
                                        },
                                        southwest: {
                                            latitude: 40,
                                            longitude: 44
                                        }
                                    }
                                };
                                self.options = {
                                    scrollwheel: true
                                };
                            }
                        });

                    };

                    self.getItemEvents = function ()
                    {
                        itemsService.getItemEvents(self.itemId).then(function (data) {
                            if (data.data.status)
                                self.events = data.data.items;
                        });
                    };
                    self.refresh = function () {
                        self.loadItem(true);
                        self.getItemEvents();
                    };


                    self.getRelatedItems = function ()
                    {
                        itemsService.getRelatedItems(self.itemId).then(function (data) {
                            if (data.data.status)
                                self.relatedItems = data.data.items;
                        });
                    };

                    self.getEmployeeByItem = function ()
                    {
                        itemsService.getEmployeeByItem(self.itemId).then(function (data) {
                            if (data.data.status)
                                self.relatedemployee = data.data.items;
                        });
                    };
                    
                    self.goToEmployItem = function(cid){
                        self.$router.navigate(['Employee',{id:cid}]);
                    }
                    
                    this.$onInit = function () {


                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };

                    self.edit = function ()
                    {

                        self.popupOen = $uibModal.open({
                            template: '<item-edit item-add-item="modalm.item" item-add-map-schema="modalm.itemAddMapSchema" item-add-map="modalm.itemAddMap" on-save="modalm.saved(message)" on-close="modalm.saved(message)"></item-edit>',
                            controllerAs: 'modalm',

                            controller: ['$http', function ($http) {
                                    var modalm = this;
                                    modalm.itemAddMapSchema = self.category.itemAddMapSchema;
                                    modalm.itemAddMap = self.category.itemAddMap;
                                    modalm.item = self.item;

                                    modalm.saved = function (message)
                                    {



                                        self.userInfoMessage = message;
                                        self.closePopup();
                                    }
                                    modalm.close = function (message)
                                    {
                                        self.userInfoError = message;
                                        self.closePopup();
                                    }

                                }]
                        });
                    };
                    self.eventFiles = function (evetnt)
                    {
                        self.popupOen = $uibModal.open({
                            template: '<m-finder-component start-path="modalm.startPath" prim="modalm.path" ></m-finder-component>',
                            controllerAs: 'modalm',

                            controller: ['$http', function ($http) {
                                    var modalm = this;
                                    modalm.path = evetnt.item_id;
                                    modalm.startPath = evetnt.short_date;
                                    console.log(modalm.path)
                                    modalm.saved = function (message)
                                    {
                                        self.userInfoMessage = message;
                                        self.closePopup();
                                    }
                                    modalm.close = function (message)
                                    {
                                        self.userInfoError = message;
                                        self.closePopup();
                                    }

                                }]
                        });
                    };

                    self.addEvent = function (item) {

                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/item/event.html',
                            controllerAs: 'modalm',

                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.selectedType = null;
                                    modalm.types = [];
                                    modalm.message = null;
                                    modalm.value = null;
                                    modalm.description = null;
                                    itemsService.getEventsType().then(function (data) {
                                        modalm.types = data.data.items;
                                    });
                                    if (typeof (item) !== 'undefined')
                                    {

                                    }
                                    modalm.onSubmit = function () {

                                        if (!modalm.selectedType || modalm.selectedType === '')
                                        {
                                            modalm.message = 'Select event type';
                                            return false;
                                        }
                                        if (!modalm.value || modalm.value === '')
                                        {
                                            modalm.message = 'Select value';
                                            return false;
                                        }
                                        var toSend = {};
                                        toSend.event_id = modalm.selectedType;
                                        toSend.item_id = self.item.id;
                                        toSend.value = modalm.value;
                                        toSend.description = modalm.description;
                                        toSend.date = modalm.date;
                                        itemsService.saveEvent(toSend).then(function (data) {
                                            if (data.data.status)
                                            {
                                                self.message = data.data.message;
                                                self.getItemEvents();
                                                self.closePopup();
                                            } else {
                                                modalm.message = data.data.message;
                                            }

                                        });

                                    };




                                }]
                        });
                    };
                    self.deleteEvent = function (item) {
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/item/eventdel.html',
                            controllerAs: 'modalm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.onSubmit = function () {

                                        var toSend = {};
                                        toSend.id = item.id;
                                        itemsService.delEvent(toSend).then(function (data) {
                                            console.log(data)
                                            if (data.data.status)
                                            {
                                                self.message = data.data.message;
                                                self.refresh();
                                                self.closePopup();
                                            } else {
                                                modalm.message = data.data.message;
                                            }
                                        });
                                    };

                                    modalm.cancel = function () {
                                        self.closePopup();
                                    }
                                }]
                        });
                    };

                    self.editEvent = function (item) {
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/item/editevent.html',
                            controllerAs: 'modalm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    console.log(item)
                                  
                                    modalm.types = [];
                                    modalm.message = null;
                                    modalm.value = item.value;
                                    modalm.description = item.description;
                                    modalm.date = item.date;
                                    modalm.selectedType = item.type_id;
                                    itemsService.getEventsType().then(function (data) {
                                        modalm.types = data.data.items;
                                    });
                                    modalm.onSubmit = function () {

                                        if (!modalm.selectedType || modalm.selectedType === '')
                                        {
                                            modalm.message = 'Select event type';
                                            return false;
                                        }
                                        if (!modalm.value || modalm.value === '')
                                        {
                                            modalm.message = 'Select value';
                                            return false;
                                        }
                                        var toSend = {};
                                        toSend.event_id = modalm.selectedType;
                                        toSend.item_id = self.item.id;
                                        toSend.value = modalm.value;
                                        toSend.description = modalm.description;
                                        toSend.date = modalm.date;
                                        toSend.id = item.id;

                                        itemsService.saveEvent(toSend).then(function (data) {
                                            
                                            if (data.data.status)
                                            {
                                                self.message = data.data.message;
                                                self.refresh();
                                                self.closePopup();
                                            } else {
                                                modalm.message = data.data.message;
                                            }
                                        });
                                    };

                                    modalm.cancel = function () {
                                        self.closePopup();
                                    }
                                }]
                        });

                    }
                    self.deleteRelatedItem = function (item) {
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/html/item/eventdel.html',
                            controllerAs: 'modalm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.onSubmit = function () {
                                        var toSend = {};
                                        toSend.dest = item.id;
                                        toSend.source = self.item.id;
                                        itemsService.deleteRelatedItem(toSend).then(function (data) {
                                           
                                            if (data.data.status)
                                            {
                                                self.message = data.data.message;
                                                self.getRelatedItems();
                                                self.closePopup();
                                            } else {
                                                modalm.message = data.data.message;
                                            }
                                        });
                                    };

                                    modalm.cancel = function () {
                                        self.closePopup();
                                    }
                                }]
                        });
                    };
                    self.relatedItemModal = function (item) {

                        self.popupOen = $uibModal.open({
                            template: '<items-modal modal-on-save="modalm.reload()" modal-item="modalm.item"></items-modal>',
                            controllerAs: 'modalm',

                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.selectedType = null;
                                    modalm.item = self.item;
                                    modalm.reload = function () {
                                        self.getRelatedItems();
                                    };



                                }]
                        });
                    };

                    self.goToItem = function (id) {
                        //console.log(id)
                        self.$router.navigate(['Item', {id: id}]);
                    };

                }]


        });