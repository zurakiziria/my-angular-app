angular.module('history-list', ['mFinder']);
angular.module('history-list')
        .component('historyList', {
            templateUrl: 'app/js/components/item/history/default.html',
            controllerAs: 'vv',
            bindings: {itemid: '<'},
            controller: ['itemsService', function (itemsService) {
                    var self = this;
                    this.$onInit = function(){
                        self.butvl = '+';
                        self.isopen = false;
                    }
                    
                    self.openHistory = function () {
                        if (self.butvl == '+') {
                            itemsService.getHistory('item', self.itemid).then(function (data) {
                                console.log(data)
                                self.ihistory = data.data;

                            });
                            itemsService.getHistory('event', self.itemid).then(function (data) {
                                console.log(data)
                                self.ehistory = data.data;

                            });
                            self.isopen = true;
                            self.butvl = '-';
                        } else {

                            self.isopen = false;
                            self.butvl = '+';
                        }
                    }

                }]
        });


