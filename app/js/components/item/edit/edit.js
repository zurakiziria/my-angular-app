angular.module('item-edit', ['mFinder']);
angular.module('item-edit')
        .component('itemEdit', {

            templateUrl: 'app/js/components/item/edit/default.html',
            controllerAs: 'vv',
            bindings: {

                "itemAddMapSchema": '<?',
                "itemAddMap": '<?',
                "itemAddItem": '<?',
                "onSave": '&',
                "onClose": '&',
            },


            controller: ['$scope', '$http', '$uibModal', 'schemaForm', 'itemsService', function ($scope, $http, $uibModal, schemaForm, itemsService) {
                    var self = this;
                    self.transfers = [];
                    self.close = function () {
                        self.closePopup();
                    };
                    $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';



                    this.$onInit = function () {
                        self.schema = self.itemAddMapSchema;
                        self.form = self.itemAddMap;
                        self.item = self.itemAddItem;
                        self.model = {};

                        if (typeof (self.item) !== 'undefined')
                        {
                            itemsService.getFormData(self.item.id).then(function (data) {
                             

                                if (data.data.status)
                                {
                                    self.model = data.data.item;
                                }
                            });
                        }
                    };





                    self.onSubmit = function (form) {
                        // First we broadcast an event so all fields validate themselves
                      //  $scope.$broadcast('schemaFormValidate');
                        // Then we check if the form is valid
                      console.log(form);
                      //console.log($scope.$broadcast('schemaFormValidate'));
                      //vakomentareb radgan redaqtirebisas zog produqtze cariel velebs $valid = false anichebda
                     //   if (form.$valid) {
                            
                            // ... do whatever you need to do with your data.
                            self.model.category_rel_id = self.item.catid;
                            self.model.relation_id = self.item.relation_id;
                           
                            itemsService.saveItem(self.model).then(function (data) {
                                if (data.data.status)
                                {
                                    self.onSave({message: data.data.message})
                                } else {
                                    self.onClose({message: data.data.message});
                                }
                            });
                     //   }
                    };
                }]

        });