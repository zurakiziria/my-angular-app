(function () {
    'use strict';
    angular.module('propertyApp').factory('categories', categories);
    categories.$inject = ['$http'];
    function categories($http) {
        return {
            getCategories: function () {
                return $http.get('/index.php?option=com_companie&task=getData'

                        );
            },
            getCategory: function (id) {
                return $http.get('/index.php?option=com_companie&task=getCategory&id='+id

                        );
            },
            getMapItems: function (data = 0) {
                
                return $http.post('/index.php?option=com_companie&type=app&task=getMapItems', data);
            },
            getItemParamses: function (id, catid) {
                return $http.post('/index.php?option=com_companie&task=getItemParamses&id=' + id + '&catid=' + catid + '');
            },
        };
    }
}());
