(function () {
    'use strict';
    angular.module('propertyApp').factory('companies', companies);
    companies.$inject = ['$http'];
    function companies($http) {
        return {
            getUserCompanies: function () {
                return $http.get('/index.php?option=com_companie&task=getUserCompanies'

                        );
            },
            setCompanySess: function (id) {
                return $http.get('/index.php?option=com_companie&task=setCompanySess&id=' + id

                        );
            },
            getCompanySess: function () {
                return $http.get('/index.php?option=com_companie&task=getCompanySess');
            }

        };
    }
}());
