(function () {
    'use strict';

    angular.module('propertyApp').factory('itemsService', itemsService);

    itemsService.$inject = ['$http', '$q', '$interval', '$window'];

    function itemsService($http, $q, $interval, $window) {
        var self = this;
        self.loadedItem = {};
        self.loadedItemForm = {};
        self.ping = function () {
            $http.get('/index.php?option=com_users&task=pingpong.ping&rnd=' + Math.random()).then(function (data) {
                if (data.status == 200 && data.data.status)
                {
                } else {
                    var host = $window.location.host;
                    var landingUrl = "http://" + host + "/index.php";
                    $window.location.href = landingUrl;
                }


            });
        };
        self.ping();
        $interval(function () {
            self.ping()

        }, 5000);

        return {

            getData: function (data) {
                return $http.post('/index.php?option=com_mymenu&type=app&task=getData', data);
            },
            getModalData: function (data) {
                return $http.post('/index.php?option=com_mymenu&type=app&task=getModalData', data);
            },
            getEmployeeHistory: function (data){
              return $http.post('/index.php?option=com_mymenu&type=app&task=getEmployeeHistory', data);
            },
            getItem: function (id, reload) {
                var nm = 'item_' + id;
                var t = typeof (reload);
                if (typeof (reload) === 'undefined')
                {
                    if (!self.loadedItem[nm]) {

                        return $http.get('/index.php?option=com_mymenu&task=getItem&id=' + id).then(function (data) {
                            self.loadedItem[nm] = data;
                            return self.loadedItem[nm];
                        });
                    }
                } else {
                    return $http.get('/index.php?option=com_mymenu&task=getItem&id=' + id).then(function (data) {
                        self.loadedItem[nm] = data;
                        return self.loadedItem[nm];
                    });
                }

                return $q.when(self.loadedItem[nm]);
            },
            clearSession: function (data) {
                return $http.post('/index.php?option=com_mymenu&type=app&task=clearSession', data);
            },
            getHistory: function (name, id) {
                return $http.post('/index.php?option=com_mymenu&task=getHistory&id=' + id + '&name=' + name);
            },

            getTemplate: function () {
                return 'app/html/item/default.html';
            },
            saveItem: function (data)
            {

                return  $http.post('/index.php?option=com_mymenu&task=saveItem', data);
            },
            getFormData: function (id)
            {
                var nm = 'item_' + id;

                if (!self.loadedItemForm[nm]) {
                    return  $http.get('/index.php?option=com_mymenu&task=getFormData&id=' + id).then(function (data) {

                        self.loadedItemForm[nm] = data;
                        return self.loadedItemForm[nm];
                    });

                }

                return $q.when(self.loadedItemForm[nm]);
            },
            getYearsByEvent: function () {
                return  $http.get('/index.php?option=com_mymenu&task=getYearsByEvent');
            },
            getEventsType: function (id)
            {
                return  $http.get('/index.php?option=com_mymenu&task=getEventTypes');
            },
            saveEvent: function (data)
            {

                return  $http.post('/index.php?option=com_mymenu&task=saveEvent', data);
            },
            saveZura: function (data)
            {
                return  $http.post('/index.php?option=com_mymenu&task=saveZura', data);
            },
            getImages: function (id)
            {
                return  $http.post('/index.php?option=com_mymenu&task=getImages&id=' + id);
            },
            linkItem: function (data)
            {
                return  $http.post('/index.php?option=com_mymenu&task=linkItem', data);
            },
            linkEmployeeItem: function (data)
            {
                return  $http.post('/index.php?option=com_mymenu&task=linkEmployeeItem', data);
            },
            delEvent: function (data)
            {
                return  $http.post('/index.php?option=com_mymenu&task=deleteEvent', data);
            },
            deleteRelatedItem: function (data)
            {
                return  $http.post('/index.php?option=com_mymenu&task=deleteRelateditem', data);
            },
            deleteEmployeeRelatedItem: function (data)
            {
                return  $http.post('/index.php?option=com_mymenu&task=deleteEmployeeRelateditem', data);
            },
            getItemEvents: function (id)
            {
                return  $http.get('/index.php?option=com_mymenu&task=getItemEvents&id=' + id);
            },
            getRelatedItems: function (id)
            {
                return  $http.get('/index.php?option=com_mymenu&task=getRelatedItems&id=' + id);
            },
            getEmployeeByItem: function (id)
            {
                return  $http.get('/index.php?option=com_mymenu&task=getEmployeeByItem&id=' + id);
            },
            getRelatedEmployeeItems: function (id)
            {
                return  $http.get('/index.php?option=com_mymenu&task=getRelatedEmployeeItems&id=' + id);
            },
            logout: function () {
                return  $http.get('/index.php?option=com_users&task=user.logout&applogouttoken=' + Math.random());
            },
            getUser: function ()
            {
                return  $http.get('/index.php?option=com_users&task=profile.getUserData&applogouttoken=' + Math.random()).then(function (data) {
                    if (data.data.status)
                    {
                        return data.data
                    } else {

                    }

                    return data;
                }).finally(
                        function (data) {
                        }
                );
            },
            eList: function (id) {
                return  $http.get('/index.php?option=com_mymenu&task=getEmployee&id='+id);
            },
            eListType: function () {
                return  $http.get('/index.php?option=com_mymenu&task=getEmployeeType');
            },
            saveEmployee: function (data) {
                return  $http.post('/index.php?option=com_mymenu&task=saveEmployee',data);
            },
            delEmployee: function (data)
            {
                return  $http.post('/index.php?option=com_mymenu&task=deleteEmployee', data);
            },
            deleteItem: function (data)
            {
                return  $http.post('/index.php?option=com_mymenu&task=deleteItem', data);
            },
        };
    }
}());
