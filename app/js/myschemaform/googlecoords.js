angular
        .module('schemaForm')
        .run(function ($templateCache) {
            // A template to use
            $templateCache.put('googlecoords.html', '<calculate sf-field-model model="model" form="form" />');
        })
        .directive('calculate', ['$compile', '$http', 'sfBuilder', 'sfSelect', '$interpolate', 'schemaFormDecorators',
            function ($compile, $http, sfBuilder, sfSelect, $interpolate, schemaFormDecoratorsProvider) {
                return {
                    template: '<div class="googleCord"><div class="googleCoordInputs"><label for="gcodeLat">{{"Lat" | translate}}</label><input id="gcodeLat" ng-paste="recount()" ng-change="recount()"  placeholder="Lat" ng-model="lat" /><label for="gcodeLng">{{"Lng" | translate}}</label><input id="gcodeLng" placeholder="Lng" ng-paste="recount()" ng-change="recount()"  ng-model="lng" /></div><div id="dfdfdfdf" style="width:100%;height:300px;">sss</div></div>', require: 'ngModel',
                    restrict: 'E',

                    scope: true,
                    link: function (scope, element, attrs, ctrl, ngModel) {

                        scope.lat = '';
                        scope.lng = '';

                        key = scope.form.key;
                        // if (scope.form)
                        var myLatLng = {lat: 41.69411, lng: 44.83368};

                        scope.gmap = initMap('dfdfdfdf');

                        scope.marker = null;
                        scope.reinitMarker = function () {
                            console.log(lng);
                        }

                        scope.gmap.addListener('click', function (a, b, c) {
                            scope.model[key] = a.latLng.lat() + '|' + a.latLng.lng();


                            if (!scope.marker)
                            {
                                scope.marker = new google.maps.Marker({
                                    position: {lat: a.latLng.lat(), lng: a.latLng.lng()},
                                    map: scope.gmap,

                                });
                            }

                            scope.marker.setPosition(new google.maps.LatLng(a.latLng.lat(), a.latLng.lng()));
                            scope.gmap.panTo(new google.maps.LatLng(a.latLng.lat(), a.latLng.lng()));
                            scope.$apply(function () {
                                scope.lat = a.latLng.lat();
                                scope.lng = a.latLng.lng();
                            });

                        });
                        scope.recount = function () {
                            if (!scope.marker)
                            {
                                //     if(scope.lat=='')
                                if (scope.lat !== '' && scope.lng !== '')
                                {
                                    console.log(scope.lat)
                                    console.log(scope.lng)
                                    scope.marker = new google.maps.Marker({
                                        position: {lat: scope.lat, lng: scope.lng},
                                        map: scope.gmap,

                                    });
                                    console.log(scope.lat)
                                    console.log(scope.lng)
                                    
                                } else
                                {
                                    return;
                                }
                            }

                            scope.marker.setPosition(new google.maps.LatLng(scope.lat, scope.lng));
                            scope.gmap.panTo(new google.maps.LatLng(scope.lat, scope.lng));
                            scope.model[key] = scope.lat + '|' + scope.lng;

                        }




                        scope.$watch(function () {
                            return scope.model;
                        },
                                function (val, old) {
                                    if (typeof (val[scope.form.key]) !== 'undefined' && val[scope.form.key] !== '')
                                    {
                                        var tmpCoords = val[scope.form.key].split('|');
                                        if (tmpCoords.length == 2 && (!isNaN(tmpCoords[0]) &&!isNaN(tmpCoords[1])))
                                        {
                                            if (!scope.marker)
                                            {
                                                scope.marker = new google.maps.Marker({
                                                    position: {lat: tmpCoords[0], lng: tmpCoords[1]},
                                                    map: scope.gmap,

                                                });
                                            }
                                            scope.marker.setPosition(new google.maps.LatLng(tmpCoords[0], tmpCoords[1]));
                                            scope.gmap.panTo(new google.maps.LatLng(tmpCoords[0], tmpCoords[1]));
                                            scope.lat = tmpCoords[0];
                                            scope.lng = tmpCoords[1];
                                            scope.$apply(function () {
                                                scope.lat = tmpCoords[0];
                                                scope.lng = tmpCoords[1];
                                            });
                                        }
                                    }

                                });

                        //update(50)
                        function update(value) {
                            sfSelect('coords', null, value)
                            scope.model[key] = value;
                        }
                        ;

                    },

                };
            },
        ])
        .config(['schemaFormDecoratorsProvider', 'sfBuilderProvider',
            function (schemaFormDecoratorsProvider, sfBuilderProvider) {
                let sfField = sfBuilderProvider.builders.sfField;
                let ngModel = sfBuilderProvider.builders.ngModel;
                let defaults = [sfField, ngModel];

                schemaFormDecoratorsProvider.defineAddOn(
                        'bootstrapDecorator',
                        'calculate',
                        'googlecoords.html',
                        defaults
                        );
            },
        ]);