//https://angular-translate.github.io/docs/#/guide/
'use strict';
(function (angular) {

    var translations = {
        "are you sure you wont delete": 'ნამდვილად გსურთ წაშლა?',
        "ok": 'კი',
        "cancle": 'გაუქმება',
        "Created": 'შეიქმნა',
        "Name": 'აქტივის დასახელება',
        name: 'დასახელება',
        description: 'აღწერა',
        DELETE: 'წაშლა',
        SEND:'გაგზავნა',
        Details: 'დეტალურად',
        Lat: 'განედი',
        Lng: 'გრძედი',
        "verbal_address": 'მისამართი',
        "address": 'ადგილმდებარეობა',
        "map": 'რუკა',
        "back to list": 'სიაში დაბრუნება',
        "Events": 'ისტორია',
        "Add Event": 'ისტორიის დამატება',
        "Submit": 'დამატება',
        "add file": 'ფაილის დამატება',
        date: 'თარიღი',
        "End price": 'საბოლოო ღირებულება',
        "End Price": 'საბოლოო ღირებულება',
        "Deprication Price": 'ცვეთადი ღირებულება',
        "Deprication": 'ცვეთა (%)',
        "Price": 'ღირებულება',
        "Add related item": 'დაკავშირებული ჩანაწერის დამატება',
        "SELECT": 'არჩევა',
        "TITLE": 'დასახელება',
        "ID": 'იდენტიფიკატორი',
        "Select date": 'აირჩიეთ თარიღი',
        "Select value": 'აირჩიეთ მნიშვნელობა',
        "Select event type": 'აირჩიეთ ოპერაციის ტიპი',
        "VALUE": 'მნიშვნელობა',
        "value": 'მნიშვნელობა',
        "enter date": 'შეიყვანეთ ტარიღი',
        "CATEGORY": 'კატეგორია',
        "QUANTITY": 'რაოდენობა',
        "S_CODE": 'საკადასტრო კოდი',
        Edit: 'რედაქტირება',
        EDIT: 'რედაქტირება',
        Show: 'ნახვა',
        Add: 'დამატება',
        FILES: 'ფაილები',
        "SELECT CATEGORY": 'კატეგორიის არჩევა',
        HEADLINE: 'What an awesome module!',
        PARAGRAPH: 'Srsly!',
        "name": 'დასახელება',
        "verbal_address": 'მისამართი',
        "history": 'ცვლილებების ისტორია',
        "related item": 'დაკავშირებული ჩანაწერები',
        "state": 'სტატუსი',
        "Start Price Sum": 'საწყისი ღირებულება',
        "End Price Sum": 'საბოლოო ღირებულება',
        "Balance Code": 'ბალანსის კოდი',
        "bcode": 'ბალანსის კოდი',
        "Deprication_price": 'ცვეთადი ღირებულება',
        "Created By": 'შემქმნელი',
        "balance_code": 'ბალანსის კოდი',
        "search_item": 'საძიებო სიტყვა',
        "please_select": 'საძიებო გასაღები',
        "related employee": 'თანამდებობის პირი',
        "Connect": 'კავშირები',
        "CONNECT": 'კავშირები',
        "List": 'სია',
        "Position": 'თანამდებობის დასახელება',
        "position": 'თანამდებობის დასახელება',
        "Identification": 'პირადობის ნომერი',
        "enter identification": 'პირადობის ნომერი',
        "enter name": 'სახელი, გვარი',
        "Employee": 'თანამშრომლები',
        "Employee name": 'თანამშრომლის სახელი',
        "added success": 'ობიექტი დამატებულია',
        "Reports": 'რეპორტები',
        "are you sure you want delete": 'დარწმუნებული ხართ, რომ გსურთ წაშლა?',
        NAMESPACE: {
            PARAGRAPH: 'And it comes with awesome features!'
        }
    };

    var app = angular.module('propertyApp');
    app.config(['$translateProvider', function ($translateProvider) {
            // add translation table
            $translateProvider
                    .translations('ka', translations)
                    .preferredLanguage('ka');
            $translateProvider.useSanitizeValueStrategy('escape');
        }]);




})(angular);

